arg version=latest
from broadinstitute/picard:$version

entrypoint ["java", "-jar", "/usr/picard/picard.jar"]
